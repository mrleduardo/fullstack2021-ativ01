package com.ativ;

public class Conta {
    private Pessoa cliente;
    private Integer nrConta;
    private Double saldo;

    // necessita de ser protected esse construtor para ser utilizado na classes filhas
    protected Conta(Pessoa cliente, Integer nrConta, Double saldo) {
        this.cliente = cliente;
        this.nrConta = nrConta;
        this.saldo = saldo;
    }

    public static Conta abrirConta(Pessoa cliente, Integer nrConta, Double saldo) {
        var conta = new Conta(cliente, nrConta, saldo);
        return conta;
    }

    public String toString(){
        return cliente.toString()+"\nnrConta: "+this.getNrConta()+"\nSaldo: "+this.getSaldo();
    }

    public Pessoa getCliente() {
        return cliente;
    }

    public Integer getNrConta() {
        return nrConta;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setCliente(Pessoa cliente) {
        this.cliente = cliente;
    }

    public void sacar(Double valor) {
        if (this.temSaldo(valor)) {
            this.saldo -= valor;
        } else {
            throw new RuntimeException("err01 - Saldo  insuficiente");
        }
    }

    protected boolean temSaldo(Double valor) {
        if (this.saldo<valor) {
            return false;
        } else {
            return true;
        }
    }

    public void deposita(Double valor) {
        this.saldo+=valor;
    }

    public void transferir(Conta destino, Double valor) {
        if (this.saldo<valor) {
            throw new RuntimeException("err01 - Saldo  insuficiente");
        }
        else{
            this.saldo -= valor;
            destino.deposita(valor);
        }
    }

}
