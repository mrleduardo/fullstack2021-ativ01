import java.util.Calendar.Builder;
import java.util.GregorianCalendar;

import com.ativ.ContaEspecial;
import com.ativ.ContaPoupanca;
import com.ativ.PessoaFisica;
import com.ativ.PessoaJuridica;

public class Main {
    public static void main(String[] args) {
        Builder builder = new GregorianCalendar.Builder();
        PessoaJuridica pess1 = new PessoaJuridica(1,"Lulz", "Rua alá", 012345677, "Tecnologia");
        PessoaFisica pess2 = new PessoaFisica(2,"Lalz", "Rua dacá", 0122345677, builder.setDate(1996, 12, 26).build(), "M");

        ContaEspecial cont1 = ContaEspecial.abrirConta(pess1, 01, 1000.25, 2000.00);
        ContaPoupanca cont2 = ContaPoupanca.abrirConta(pess1, 01, 1234.56, 0.14);
        ContaPoupanca cont3 = ContaPoupanca.abrirConta(pess2, 02, 1234.56, 0.12);

        System.out.println("==============");
        System.out.println(cont1.toString());
        System.out.println("==============");
        System.out.println(cont2.toString());
        System.out.println("==============");
        System.out.println(cont3.toString());
        System.out.println("==============");
    }
}
